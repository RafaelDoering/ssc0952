################################################################################
##                                   COMMANDS                                 ##
################################################################################

COMPOSE := docker compose
COMPOSE_BUILD := $(COMPOSE) build

################################################################################
##                               LINT & FORMAT                                ##
################################################################################

isort:
	@$(call message,"Rodando isort")
	@$(COMPOSE) run -T --rm --entrypoint isort api iot
	@$(COMPOSE) run -T --rm --entrypoint isort api tests

black:
	@$(call message,"Rodando black")
	@$(COMPOSE) run -T --rm --entrypoint black api iot
	@$(COMPOSE) run -T --rm --entrypoint black api tests

autoflake:
	@$(call message,"Rodando autoflake")
	@$(COMPOSE) run -T --rm --entrypoint autoflake api \
		--in-place --remove-all-unused-imports --remove-unused-variables \
		--ignore-init-module-imports --expand-star-imports --recursive \
		iot
	@$(COMPOSE) run -T --rm --entrypoint autoflake api \
		--in-place --remove-all-unused-imports --remove-unused-variables \
		--ignore-init-module-imports --expand-star-imports --recursive \
		tests

mypy:
	@$(call message,"Rodando mypy")
	@$(COMPOSE) run -T --rm --entrypoint mypy api iot
	@$(COMPOSE) run -T --rm --entrypoint mypy api tests

flake8:
	@$(call message,"Rodando flake8")
	@$(COMPOSE) run -T --rm --entrypoint flake8 api iot
	@$(COMPOSE) run -T --rm --entrypoint flake8 api tests

lint:
	@$(MAKE) mypy
	@$(MAKE) flake8

format:
	@$(MAKE) black
	@$(MAKE) isort
	@$(MAKE) autoflake

################################################################################
##                                LOCAL EXECUTION                             ##
################################################################################

mosquitto:
	@$(COMPOSE) up -d mosquitto

mongodb:
	@$(COMPOSE) up -d mongodb

app-build:
	@$(COMPOSE) up -d app-build

app:
	@$(COMPOSE) up -d app

start:
	@$(MAKE) mosquitto
	@$(MAKE) mongodb
	@$(MAKE) app-build
	@$(MAKE) app
	@$(COMPOSE) up --build -d api

dev:
	@$(MAKE) mosquitto
	@$(MAKE) mongodb
	@$(MAKE) app-build
	@$(MAKE) app
	@$(COMPOSE) up --build api

stop:
	@$(COMPOSE) down -v

test:
	@$(COMPOSE_BUILD)
	@$(COMPOSE) run --rm --entrypoint pytest api --asyncio-mode=auto -vv
