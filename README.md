SSC0952
===

Lucas Henrique Braga Martins - 11275126

Israel Felipe da Silva - 11796531

Mateus dos Dantos Ribeiro - 11796997

Rafael Doering Soares - 10408410


## Executando localmente através do docker

O projeto pode ser iniciada:

```shell
make start
```

## Executando testes

```shell
make test
```

## Executando formatador de código e linter

```shell
make format
make lint
```
