from abc import ABC, abstractmethod
from typing import Optional


class Storage(ABC):
    @abstractmethod
    async def read_one(self, table_name: str, key: str) -> Optional[dict]:
        pass

    @abstractmethod
    async def write_one(self, table_name: str, key: str, entity: dict) -> None:
        pass
