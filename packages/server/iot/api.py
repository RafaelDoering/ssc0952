from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .adapters import observability_router
from .adapters.broker import Broker
from .adapters.car_router import CarRouter
from .adapters.light_router import LightRouter

from .enviroment_configs import Environment

api = FastAPI()

api.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

def main():
    env = Environment()

    api.include_router(observability_router.router)

    broker = Broker(env.mosquitto_host, env.mosquitto_port)
    car_router = CarRouter(broker)
    api.include_router(car_router.create())

    light_router = LightRouter(broker)
    api.include_router(light_router.create())

    print("Servidor iniciado")


main()
