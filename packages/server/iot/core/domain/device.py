from iot.core.domain.base_entity import BaseEntity


class Device(BaseEntity):
    name: str

    def __init__(
        self,
        name: str,
        created_at: int = None,
        updated_at: int = None,
        id: str = None,
    ) -> None:
        super().__init__(id=id, created_at=created_at, updated_at=updated_at)
        self.name = name
