from dataclasses import dataclass


@dataclass
class BaseEntity:
    id: str
    created_at: int
    updated_at: int

    def __init__(
        self,
        id: str = None,
        created_at: int = None,
        updated_at: int = None,
    ) -> None:
        if id is not None:
            self.id = id
        if created_at is not None:
            self.created_at = created_at
        if updated_at is not None:
            self.updated_at = updated_at
