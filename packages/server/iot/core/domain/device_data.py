from iot.core.domain.base_entity import BaseEntity


class DeviceData(BaseEntity):
    device_id: str

    def __init__(
        self,
        device_id: str,
        created_at: int = None,
        updated_at: int = None,
        id: str = None,
    ) -> None:
        super().__init__(id=id, created_at=created_at, updated_at=updated_at)
        self.device_id = device_id
