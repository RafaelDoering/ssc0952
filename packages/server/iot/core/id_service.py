from uuid import uuid4


class IdService:
    async def create(self) -> str:
        return str(uuid4())
