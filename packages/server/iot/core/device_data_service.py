from iot.core.domain.device_data import DeviceData
from iot.core.domain.exceptions import NotFound
from iot.core.id_service import IdService
from iot.ports.storage import Storage

TABLE_NAME = "device-data"


class DeviceDataService:
    def __init__(
        self,
        storage: Storage,
        id_service: IdService,
    ) -> None:
        self.storage = storage
        self.id_service = id_service

    async def create(self, device: DeviceData) -> DeviceData:
        device.id = await self.id_service.create()
        await self.storage.write_one(TABLE_NAME, device.id, device.__dict__)
        return device

    async def get_one(self, id: str) -> DeviceData:
        device_dict = await self.storage.read_one(TABLE_NAME, id)

        if device_dict is None:
            raise NotFound()

        device = DeviceData(device_dict["device_id"], device_dict["id"])

        return device
