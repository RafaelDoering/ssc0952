import time


class TimeService:
    def get_current_timestamp(self) -> int:
        return time.time_ns()
