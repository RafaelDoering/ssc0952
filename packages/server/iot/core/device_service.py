from iot.core.domain.device import Device
from iot.core.domain.exceptions import NotFound
from iot.core.id_service import IdService
from iot.core.time_service import TimeService
from iot.ports.storage import Storage

TABLE_NAME = "device"


class DeviceService:
    def __init__(
        self,
        storage: Storage,
        id_service: IdService,
        time_service: TimeService,
    ) -> None:
        self.storage = storage
        self.id_service = id_service
        self.time_service = time_service

    async def create(self, device: Device) -> Device:
        current_time = self.time_service.get_current_timestamp()

        device.id = await self.id_service.create()
        device.created_at = current_time
        device.updated_at = current_time

        await self.storage.write_one(TABLE_NAME, device.id, device.__dict__)

        return device

    async def get_one(self, id: str) -> Device:
        device_dict = await self.storage.read_one(TABLE_NAME, id)

        if device_dict is None:
            raise NotFound()

        device = Device(
            id=device_dict["id"],
            name=device_dict["name"],
            created_at=device_dict["created_at"],
            updated_at=device_dict["updated_at"],
        )

        return device
