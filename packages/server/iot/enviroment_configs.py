from pydantic import BaseSettings


class Environment(BaseSettings):
    mosquitto_host: str
    mosquitto_port: int
    mongodb_username: str
    mongodb_password: str
    mongodb_hostname: str
    mongodb_port: int
    mongodb_database: str


env = Environment()
