from typing import Callable

import paho.mqtt.client as mqtt


class Broker:
    def __init__(self, host: str, port: int) -> None:
        self.client = mqtt.Client()

        self.client.connect(host, port, 60)
        self.client.loop_start()

    def publish(self, topic_name: str, message: str):
        self.client.publish(topic=topic_name, payload=message, retain=False)

    def consume(self, topic_name: str, on_message: Callable):
        self.client.on_connect = self._on_connect
        self.client.on_message = on_message
        self.client.subscribe(topic=topic_name)

    def _on_connect(self, client, userdata, flags, rc):
        print("Conectado ao broker")
