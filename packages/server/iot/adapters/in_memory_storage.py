from typing import Any, Optional

from iot.ports.storage import Storage


class InMemoryStorage(Storage):
    def __init__(self):
        self.storage: dict[str, dict[str, Any]] = {}

    async def read_one(self, table_name: str, key: str) -> Optional[dict]:
        if table_name not in self.storage or key not in self.storage[table_name]:
            return None

        return self.storage[table_name][key]

    async def write_one(self, table_name: str, key: str, entity: dict) -> None:
        if table_name not in self.storage:
            self.storage[table_name] = {}

        self.storage[table_name][key] = entity
