from typing import Any

from fastapi import APIRouter, status


router = APIRouter()


class CarRouter:
    def __init__(self, broker: Any) -> None:
        self.broker = broker

    def create(self) -> APIRouter:
        @router.get("/forward", status_code=status.HTTP_200_OK)
        async def handle_forward_request() -> str:
            self.broker.publish("esp32/car", "forward")

            return "OK"

        @router.get("/backward", status_code=status.HTTP_200_OK)
        async def handle_backward_request() -> str:
            self.broker.publish("esp32/car", "backward")

            return "OK"

        @router.get("/left", status_code=status.HTTP_200_OK)
        async def handle_left_request() -> str:
            self.broker.publish("esp32/car", "left")

            return "OK"

        @router.get("/right", status_code=status.HTTP_200_OK)
        async def handle_right_request() -> str:
            self.broker.publish("esp32/car", "right")

            return "OK"

        @router.get("/stop", status_code=status.HTTP_200_OK)
        async def handle_stop_request() -> str:
            self.broker.publish("esp32/car", "stop")

            return "OK"

        return router
