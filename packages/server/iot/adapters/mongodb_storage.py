from typing import Optional

from pymongo import MongoClient

from iot.ports.storage import Storage


class MongodbStorage(Storage):
    client: MongoClient

    def __init__(
        self,
        hostname: str,
        username: str,
        password: str,
        port: int = 27017,
        database: str = "test",
    ):
        params = "?authSource=admin"
        host = f"mongodb://{username}:{password}@{hostname}:{port}/{database}{params}"

        self.client = MongoClient(host=host)
        self.db = self.client[database]

    async def read_one(self, table_name: str, key: str) -> Optional[dict]:
        return self.db[table_name].find_one({"id": key})

    async def write_one(self, table_name: str, key: str, entity: dict) -> None:
        entity_with_id = {
            **entity,
            "id": key,
        }
        self.db[table_name].insert_one(entity_with_id)
