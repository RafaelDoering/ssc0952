from typing import Any

from fastapi import APIRouter, status


router = APIRouter()


class LightRouter:
    def __init__(self, broker: Any) -> None:
        self.broker = broker

    def create(self) -> APIRouter:
        @router.get("/on", status_code=status.HTTP_200_OK)
        async def handle_on_request() -> str:
            self.broker.publish("esp32/light", "on")

            return "OK"

        @router.get("/off", status_code=status.HTTP_200_OK)
        async def handle_off_request() -> str:
            self.broker.publish("esp32/light", "off")

            return "OK"

        return router
