from fastapi import APIRouter, status

router = APIRouter()


@router.get("/healthz", status_code=status.HTTP_200_OK)
async def healthcheck() -> dict:
    return {"status": "OK"}
