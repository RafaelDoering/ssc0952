from iot.core.id_service import IdService

id_service = IdService()


async def test_create_id_returns_when_called():
    id = await id_service.create()

    assert isinstance(id, str) is True
