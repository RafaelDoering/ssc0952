from iot.core.time_service import TimeService

time_service = TimeService()


async def test_get_current_timestamp_returns_int_when_called():
    current_timestamp = time_service.get_current_timestamp()

    assert isinstance(current_timestamp, int) is True
