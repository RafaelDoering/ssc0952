import pytest

from iot.adapters.in_memory_storage import InMemoryStorage
from iot.core.device_service import DeviceService
from iot.core.domain.device import Device
from iot.core.domain.exceptions import NotFound
from iot.core.id_service import IdService
from iot.core.time_service import TimeService


@pytest.fixture()
def device_service() -> DeviceService:
    storage = InMemoryStorage()
    id_service = IdService()
    time_service = TimeService()

    return DeviceService(
        storage=storage,
        id_service=id_service,
        time_service=time_service,
    )


async def test_create_returns_entity_when_success(device_service: DeviceService):
    device = await device_service.create(Device(name="test"))

    assert device.name == "test"
    assert isinstance(device.created_at, int) is True
    assert isinstance(device.updated_at, int) is True
    assert isinstance(device.id, str) is True


async def test_get_one_returns_device_when_exist(device_service: DeviceService):
    created_device = await device_service.create(Device(name="test"))
    device = await device_service.get_one(created_device.id)

    assert device is not None
    assert device.name == "test"
    assert isinstance(device.created_at, int) is True
    assert isinstance(device.updated_at, int) is True


async def test_get_one_raises_not_found_when_does_not_exist(
    device_service: DeviceService,
):
    with pytest.raises(NotFound):
        await device_service.get_one("123")
