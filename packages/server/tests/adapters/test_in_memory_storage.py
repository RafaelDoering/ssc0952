from iot.adapters.in_memory_storage import InMemoryStorage

table_name = "test"


async def test_write_and_read_one():
    storage = InMemoryStorage()

    entity = {
        "key": "1",
        "value": {
            "value": "1",
        },
    }

    await storage.write_one(table_name, entity["key"], entity["value"])
    values = await storage.read_one(table_name, entity["key"])

    assert values == entity["value"]


async def test_read_when_entry_not_found():
    storage = InMemoryStorage()

    entity1 = {
        "key": "1",
        "value": {
            "value": "1",
        },
    }
    entity2 = {
        "key": "2",
        "value": {
            "value": "2",
        },
    }

    await storage.write_one(table_name, entity1["key"], entity1["value"])

    values = await storage.read_one(table_name, entity2["key"])

    assert values is None
