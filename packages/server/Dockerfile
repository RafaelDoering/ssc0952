FROM python:3.10-slim

WORKDIR /usr/src/

ENV PYTHONUNBUFFERED=1 \
    PIP_NO_CACHE_DIR=off \
    POETRY_VERSION=1.2.1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR=/usr/src/poetry_cache/ \
    HOST=0.0.0.0 \
    PORT=8001

ENTRYPOINT uvicorn --host ${HOST} --port ${PORT} --log-level warning iot.api:api

RUN apt-get update -qq && apt-get -y -qq install \
    python-dev \
    cmake \
    libgomp1

RUN pip install "poetry==${POETRY_VERSION}"

COPY pyproject.toml poetry.lock ./

RUN poetry install --no-interaction --no-ansi --no-cache --no-root

COPY . .
