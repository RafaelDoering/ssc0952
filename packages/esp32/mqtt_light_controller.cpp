#include <Arduino.h>
#include <WiFi.h>

#include <ESPAsyncWebServer.h>

#include "light_service.h"

void lightController(String message) {
  if (message == "on") {
    Serial.println("led on");
    led_on();
  } else if (message == "off") {
    Serial.println("led off");
    led_off();
  }
}
