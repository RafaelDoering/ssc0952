#include <Arduino.h>

#include "car_service.h"

void carController(String message) {
  if (message == "forward") {
    Serial.println("forward");
    forward();
  } else if (message == "backward") {
    Serial.println("backward");
    backward();
  } else if (message == "left") {
    Serial.println("left");
    left();
  } else if (message == "right") {
    Serial.println("right");
    right();
  } else if (message == "stop") {
    Serial.println("stop");
    stop();
  }
}
